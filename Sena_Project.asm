.MODEL SMALL
.CODE
Org 100h
MOV AH,02H          ;nilai servis untuk mencetak karakter
MOV DL,'J'          ;Karakter ASCII yang akan dicetak
INT 21H             ;mencetak karakter
MOV DL,'A'
INT 21H
MOV DL,'D'
INT 21H
MOV DL,'W'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'L'
INT 21H
MOV DL,' '
INT 21H

MOV DL,'K'
INT 21H
MOV DL,'U'
INT 21H
MOV DL,'L'
INT 21H
MOV DL,'I'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'H'
INT 21H
MOV DL,' '
INT 21H

MOV DL,'T'
INT 21H
MOV DL,'.'
INT 21H
MOV DL,'I'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'F'
INT 21H
MOV DL,'O'
INT 21H
MOV DL,'R'
INT 21H
MOV DL,'M'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'T'
INT 21H
MOV DL,'I'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H

MOV DL,' '
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'E'
INT 21H
MOV DL,'L'
INT 21H
MOV DL,'A'
INT 21H 
MOV DL,'S'
INT 21H

MOV DL,' '
INT 21H
MOV DL,'C'
INT 21H

MOV DL,' '
INT 21H
MOV DL,'2'
INT 21H
MOV DL,'0'
INT 21H
MOV DL,'2'
INT 21H
MOV DL,'1'
INT 21H
MOV DL,0DH
INT 21H
MOV DL,0AH
INT 21H
MOV DL,0DH
INT 21H

;KETERANGAN KELOMPOK 1
MOV DL,'T'
INT 21H
MOV DL,'E'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,' '
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'G'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'('
INT 21H
MOV DL,'1'
INT 21H
MOV DL,')'
INT 21H
MOV DL,'='
INT 21H
MOV DL,'M'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'T'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'U'
INT 21H
MOV DL,'L'
INT 21H
MOV DL,0DH
INT 21H
MOV DL,0AH
INT 21H
;KETERANGAN KELOMPOK 2
MOV DL,'T'
INT 21H
MOV DL,'E'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,' '
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'N'
INT 21H
MOV DL,'G'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'('
INT 21H
MOV DL,'2'
INT 21H
MOV DL,')'
INT 21H
MOV DL,'='
INT 21H
MOV DL,'P'
INT 21H
MOV DL,'R'
INT 21H
MOV DL,'A'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'T'
INT 21H
MOV DL,'I'
INT 21H
MOV DL,'K'
INT 21H
MOV DL,'U'
INT 21H
MOV DL,'M'
INT 21H
MOV DL,' '
INT 21H
MOV DL,0DH      ;karakter enter
INT 21H         ;cetak karakter
MOV DL,0AH      ;karakter enter
INT 21H         ;cetak karakter
MOV DL,0DH
INT 21H
MOV DL,0AH
INT 21H


jmp mulai
                                                                      
msg1: db 0dh,0ah,"*SENIN   :| Pancasila & Al Quran Hadis| 08.45-10.25 & 12.30-14.10 | Grup WA |",0dh,0ah,"*SELASA :| DDP & Logika | 09.35-12.10 & 12.30-14.30 | Gclass,Gmeet |",0dh,0ah,"*RABU   :| Dasar Sistem Komputer | 12.30-14.10 | Elearning |",0dh,0ah,"*KAMIS  :| MDI | 09.35-12.10 | Gclass,Gmeet |",0dh,0ah,"*JUMAT  :| Tidak ada kelas |",0dh,0ah,"*SABTU  :| Kalkulus | 08.45-12.10 | Gclass,Gmeet |",0dh,0ah,'$'
msg2: db 0dh,0ah,"*SENIN   :| Tidak ada praktikum |",0dh,0ah,"*SELASA :| Praktikum Dasar Sistem Komputer | 07.30-08.45 | Gclass,Gmeet |",0dh,0ah,"*RABU   :| Tidak Ada Praktikum |",0dh,0ah,"*KAMIS  :| Praktikum Logika Informatika | 13.30-14.30 | Gclass,Gmeet |",0dh,0ah,"*JUMAT  :| Tidak ada praktikum |",0dh,0ah,"*SABTU  :| Praktikum Dasar Pemrograman | 10.30-12.30 | Gclass,Gmeet |",0dh,0ah,'$'

                                             
mulai:

mov ah, 01      ;nilai servis di ah dg 01  
int 21h         ;cetak karakter

cmp al, '1'     ; bandingkan al dengan 01
je kel_1        ; lompat ke kelompok_1   jika sama
    
cmp al, '2'     ;bandingkan al dengan 2
je kel_2        ; lompat ke kelompok_2   jika sama


jmp exit        ;lompat ke exit
exit:           ;exit program
ret

kel_1:
    mov dx, msg1    ;ambil offset msg1
    mov ah, 9       ;servis untuk cetak kalimat
    int 21h         ;cetak kalimat
    jmp mulai       ;lompat ke mulai
kel_2:
    mov dx, msg2    ;ambil offset msg1
    mov ah, 9       ;servis untuk cetak kalimat
    int 21h         ;cetak kalimat
    jmp mulai       ;lompat ke mulai